package net.lugdunon.world.defaults.compendium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.core.console.parameters.IParameterFilter;
import net.lugdunon.command.core.console.parameters.RegexBasedParameterFilter;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.compendium.Compendium;
import net.lugdunon.state.item.Item;

public class InstancedPlayerItemLinkParameterFilter extends RegexBasedParameterFilter implements IParameterFilter
{
	@Override
	public String getName()
	{
		return("Instantiated Player Item Link");
	}

	@Override
	public String getDescription()
	{
		Compendium c=State.instance().getWorld().getCompendium();
		
		return("Replaces text with a link to the specified instantiated player item's entry in the '"+c.getCodex("instantiated.player.items").getName()+"' codex of the '"+c.getName()+"'.");
	}

	@Override
	public String getMatch()
	{
		return("(ITEM:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)");
	}

	@Override
	public String getHRMatch()
	{
		return("ITEM:ITEM.ID");
	}
	
	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public String postProcess(String command, Character actor, CommandProperties props)
    {
		Pattern      p =getPattern(       );
	    Matcher      m =p.matcher (command);
	    StringBuffer sb=new StringBuffer ();
	    Item         i;
	    
	    while(m.find())
	    {
	    	i=State.instance().getWorld().getItemDefinitions().getItemDef(m.group(2));
	    	
	    	if(i != null && !i.isNonInstanced() && !i.isNpcOnly())
	    	{
		        m.appendReplacement(
		        	sb, 
		        	"<a onclick=\"game.compendium.show('instantiated.player.items','"+
		        	i.getItemId()+
		        	"');\">Item: "+
		        	i.getName()+
		        	"</a>"
		        );
	    	}
	    }
	    
		return(sb.toString());
    }
}
