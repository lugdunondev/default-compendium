package net.lugdunon.world.defaults.compendium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.core.console.parameters.IParameterFilter;
import net.lugdunon.command.core.console.parameters.RegexBasedParameterFilter;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.compendium.Compendium;
import net.lugdunon.state.item.Item;

public class SpellLinkParameterFilter extends RegexBasedParameterFilter implements IParameterFilter
{
	public SpellLinkParameterFilter()
	{
		setPattern(Pattern.compile(getMatch(),Pattern.CASE_INSENSITIVE));
	}
	
	@Override
	public String getName()
	{
		return("Spell Link");
	}

	@Override
	public String getDescription()
	{
		Compendium c=State.instance().getWorld().getCompendium();
		
		return("Replaces text with a link to the specified spell's entry in the '"+c.getCodex("spells").getName()+"' codex of the '"+c.getName()+"'.");
	}

	@Override
	public String getMatch()
	{
		return("(SPELL:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)");
	}

	@Override
	public String getHRMatch()
	{
		return("SPELL:SPELL.ID");
	}
	
	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public String postProcess(String command, Character actor, CommandProperties props)
    {
		Pattern      p =getPattern(       );
	    Matcher      m =p.matcher (command);
	    StringBuffer sb=new StringBuffer ();
	    Item         i;
	    
	    while(m.find())
	    {
	    	i=State.instance().getWorld().getItemDefinitions().getItemDef(m.group(2));
	    	
	    	if(i != null && i.isSpell())
	    	{
		        m.appendReplacement(
		        	sb, 
		        	"<a onclick=\"game.compendium.show('spells','"+
		        	i.getItemId()+
		        	"');\">Spell: "+
		        	i.getName()+
		        	"</a>"
		        );
	    	}
	    }
	    
		return(sb.toString());
    }
}
