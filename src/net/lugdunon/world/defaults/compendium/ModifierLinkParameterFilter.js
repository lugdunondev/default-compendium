Namespace.declare("net.lugdunon.world.defaults.compendium");

Namespace.newClass("net.lugdunon.world.defaults.compendium.ModifierLinkParameterFilter","net.lugdunon.command.core.console.parameters.IParameterFilter");

// //

net.lugdunon.world.defaults.compendium.ModifierLinkParameterFilter.prototype.init=function(initData)
{
	this.matcher    =/(MODIFIER:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)/gm;
	this.replacement=function(match, mod, modId)
	{
		return(
        	"<a onclick=\"game.compendium.show('modifiers','"+
        	modId+
        	"');\">Modifier: " +
        	game.modifiers[modId].name+
        	"</a>"
		);
	};

	return(this);
};

net.lugdunon.world.defaults.compendium.ModifierLinkParameterFilter.prototype.process=function(text, actor, props)
{
	return(
		text.replace(
			this.matcher,
			this.replacement
		)
	);
};