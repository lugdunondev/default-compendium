Namespace.declare("net.lugdunon.world.defaults.compendium");

Namespace.newClass("net.lugdunon.world.defaults.compendium.SpellLinkParameterFilter","net.lugdunon.command.core.console.parameters.IParameterFilter");

// //

net.lugdunon.world.defaults.compendium.SpellLinkParameterFilter.prototype.init=function(initData)
{
	this.matcher    =/(SPELL:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)/gm;
	this.replacement=function(match, spell, spellId)
	{
		return(
        	"<a onclick=\"game.compendium.show('spells','"+
        	spellId+
        	"');\">Spell: "+
        	game.items[spellId].name+
        	"</a>"
		);
	};

	return(this);
};

net.lugdunon.world.defaults.compendium.SpellLinkParameterFilter.prototype.process=function(text, actor, props)
{
	return(
		text.replace(
			this.matcher,
			this.replacement
		)
	);
};