package net.lugdunon.world.defaults.compendium.versions;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.Server;
import net.lugdunon.server.PropertyDefinition;
import net.lugdunon.server.mod.ServerMod;
import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;
import net.lugdunon.state.mod.Mod;
import net.lugdunon.util.credit.Credit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Version Information");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of all server version information.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		JSONObject o=new JSONObject();
		JSONArray  a=new JSONArray ();
		JSONObject p=new JSONObject();
		
		o.put("server", p                 );
		p.put("version",Server.getVersion());
		
		// WORLD MODS
		o.put("mods",a);
		
		for(Mod m:State.instance().getWorld().getModCache().listMods())
		{
			p=new JSONObject();

			p.put("id",         m.getId         ());
			p.put("name",       m.getName       ());
			p.put("description",m.getDescription());
			p.put("version",    m.getVersion    ());
			
			if(m.getCredits() != null)
			{
				JSONArray c=new JSONArray();
				
				for(Credit credit:m.getCredits())
				{
					c.put(credit.toJSONObject());
				}
				
				p.put("credits",c);
			}
			
			a.put(p);
		}
		
		// SERVER MODS
		a=new JSONArray();
		o.put("server.mods",a);
		
		for(ServerMod sm:Server.getServerModCache().listServerMods())
		{
			p=new JSONObject();

			p.put("id",         sm.getId         ());
			p.put("name",       sm.getName       ());
			p.put("description",sm.getDescription());
			p.put("version",    sm.getVersion    ());
			
			if(sm.getCredits() != null)
			{
				JSONArray c=new JSONArray();
				
				for(Credit credit:sm.getCredits())
				{
					c.put(credit.toJSONObject());
				}
				
				p.put("credits",c);
			}
			
			a.put(p);
		}
		
		// WORLD PROPERTIES
		a=new JSONArray();
		o.put("world.properties",a);
		
		for(PropertyDefinition pd:Server.listRegisteredWorldProperties())
		{
			p=pd.toJSONObject();
			
			if(State.instance().getWorld().getWorldConfigProperty(pd.getKey()) != null)
			{
				p.put("value", State.instance().getWorld().getWorldConfigProperty(pd.getKey()));
			}
			
			a.put(p);
		}

		// SERVER PROPERTIES
		a=new JSONArray();
		o.put("server.properties",a);
		
		for(PropertyDefinition pd:Server.listRegisteredServerProperties())
		{
			p=pd.toJSONObject();
			
			if(Server.getServerProperty(pd.getKey()) != null)
			{
				p.put("value", Server.getServerProperty(pd.getKey()));
			}
			
			a.put(p);
		}
		
	    return(o);
    }
}