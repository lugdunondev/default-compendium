Namespace.declare("net.lugdunon.world.defaults.compendium.versions");
Namespace.newClass("net.lugdunon.world.defaults.compendium.versions.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.versions.Codex,"init",[initData]);
	this.data=null;
	
	return(this);
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.fetched=function()
{
	this.renderServer          (this.data["server"           ]);
	this.renderServerMods      (this.data["server.mods"      ]);
	this.renderMods            (this.data["mods"             ]);
	this.renderServerProperties(this.data["server.properties"]);
	this.renderWorldProperties (this.data["world.properties" ]);
	
	this.parent.append("<div style='width:16px;height:16px;'>&nbsp;</div>");
	
	this.callSuper(net.lugdunon.world.defaults.compendium.versions.Codex,"fetched",[]);
};

////

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.decorateIcon=function(context)
{
	if(this.data && this.data.server)
	{
		var w;
		
		context.fillStyle  ="#FFF";
		context.strokeStyle="#000";
		context.lineWidth  =2;
		context.font       ="bold 12px sans-serif";
		
		w                  =context.measureText(this.data.server.version).width;

		context.strokeText(this.data.server.version,42-(w+4),38);
		context.fillText  (this.data.server.version,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderServer=function(server)
{
	this.parent.append("<div class='versionLabel'>Server Version:</div><div class='versionValue'>"+server.version+"</div>");
	this.parent.append("<div class='versionLabel'>Client Version:</div><div class='versionValue'>"+Namespace.loadItem("./assets/","manifest.version")+"</div>");
	
	if(game.isStandalone())
	{
		this.parent.append("<div class='versionLabel'>Standalone Client Version:</div><div class='versionValue'>"+game.client.standaloneClientVersion+"</div>");
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderTooltip=function(data)
{
	//(SERVER) MODS
	if(data.version)
	{
		var c="";
		
		if(data.credits)
		{
			c+="<div style='height:16px;' class='ttLineItem'></div>"+
			   "<div class='ttLineItem'>Credits:</div>";
			
			for(var i=0;i<data.credits.length;i++)
			{
				c+="<div style='height:16px;' class='ttLineItem'></div>";
				c+="<div style='margin-left:16px;color:#0F0;font-style:bold;' class='ttLineItem'>"+data.credits[i].type+"</div>";
				
				for(var j=0;j<data.credits[i].attributions.length;j++)
				{
					c+="<div style='margin-left:32px;color:#0F0;' class='ttLineItem'>"+
					   data.credits[i].attributions[j].name+
					   (
						   (data.credits[i].attributions[j].url != null && data.credits[i].attributions[j].url != "")?
						   (" ("+data.credits[i].attributions[j].url+")"):
						   ("")
					   )+
					   "</div>";
					
					if(data.credits[i].attributions[j].particulars != null && data.credits[i].attributions[j].particulars != "")
					{
						c+="<div style='margin-left:48px;color:#0F0;' class='ttLineItem'>- "+data.credits[i].attributions[j].particulars+"</div>";
					}
				}
			}
		}
		 
		return(
			{
				w:384,
				h:0,
				html:
					"<div class='ttName'>"       +data.name       +"</div>"+
					"<div class='ttName'>"       +data.id         +"</div>"+
					"<div class='ttLineItem'>v"  +data.version    +"</div>"+
					"<div class='ttDescription'>"+data.description+"</div>"+
					c
			}
		);
	}
	//SERVER / WORLD PROPERTIES
	else
	{
		var v=((data.value!=null)?(data.value):((data.default!=null)?(data.default):("")));
		var d=                                 ((data.default!=null)?(data.default):("")) ;

		v=v==""?"&nbsp;":v;
		d=d==""?"&nbsp;":d;
		
		return(
			{
				w:384,
				h:0,
				html:
					"<div class='ttName'>"            +data.name       +"</div>"+
					"<div class='ttDescription'>"     +data.description+"</div>"+
					"<div class='ttLineItem'>&nbsp;</div>"+
					"<div class='ttLineItem'>Key:</div>"+
					"<div class='ttSubLineItem'>"     +data.key        +"</div>"+
					"<div class='ttLineItem'>&nbsp;</div>"+
					"<div class='ttLineItem'>Value:</div>"+
					"<div class='ttSubLineItem'>"     +v               +"</div>"+
					"<div class='ttLineItem'>&nbsp;</div>"+
					"<div class='ttLineItem'>Default:</div>"+
					"<div class='ttSubLineItem'>"     +d               +"</div>"
			}
		);
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderServerMods=function(serverMods)
{
	var c;
	
	this.parent.append("<div class='versionTitle'>Server Mods</div>");
	
	for(var i=0;i<serverMods.length;i++)
	{
		c=$("<div class='versionRow'><div class='versionLabel'>"+serverMods[i].id+":</div><div class='versionValue'>"+serverMods[i].version+"</div></div>");

		game.onShowHideTooltip(
			c,
			this,
			serverMods[i]
		);
		
		this.parent.append(c);
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderMods=function(mods)
{
	var c;
	
	this.parent.append("<div class='versionTitle'>World Mods</div>");
	
	for(var i=0;i<mods.length;i++)
	{
		c=$("<div class='versionRow'><div class='versionLabel'>"+mods[i].id+":</div><div class='versionValue'>"+mods[i].version+"</div></div>");

		game.onShowHideTooltip(
			c,
			this,
			mods[i]
		);
		
		this.parent.append(c);
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderServerProperties=function(serverProperties)
{
	var c;
	
	this.parent.append("<div class='versionTitle'>Server Properties</div>");
	
	for(var i=0;i<serverProperties.length;i++)
	{
		c=$(
			"<div class='versionRow'><div class='versionPropertiesLabel'>"+
			serverProperties[i].key+
			"</div><div class='versionPropertiesValue'>"+
			((serverProperties[i].value!=null)?(serverProperties[i].value):((serverProperties[i].default!=null)?(serverProperties[i].default):("")))+
			"</div></div>"
		);

		game.onShowHideTooltip(
			c,
			this,
			serverProperties[i]
		);
		
		this.parent.append(c);
	}
};

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.renderWorldProperties=function(worldProperties)
{
	var c;
	
	this.parent.append("<div class='versionTitle'>World Properties</div>");
	
	for(var i=0;i<worldProperties.length;i++)
	{
		c=$(
			"<div class='versionRow'><div class='versionPropertiesLabel'>"+
			worldProperties[i].key+
			"</div><div class='versionPropertiesValue'>"+
			((worldProperties[i].value!=null)?(worldProperties[i].value):((worldProperties[i].default!=null)?(worldProperties[i].default):("")))+
			"</div></div>"
		);

		game.onShowHideTooltip(
			c,
			this,
			worldProperties[i]
		);
		
		this.parent.append(c);
	}
};

////

net.lugdunon.world.defaults.compendium.versions.Codex.prototype.highlight=function(data)
{
	;
};