package net.lugdunon.world.defaults.compendium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.core.console.parameters.IParameterFilter;
import net.lugdunon.command.core.console.parameters.RegexBasedParameterFilter;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.compendium.Compendium;

import org.json.JSONObject;

public class ModifierLinkParameterFilter extends RegexBasedParameterFilter implements IParameterFilter
{
	public ModifierLinkParameterFilter()
	{
		setPattern(Pattern.compile(getMatch(),Pattern.CASE_INSENSITIVE));
	}
	
	@Override
	public String getName()
	{
		return("Modifier Link");
	}

	@Override
	public String getDescription()
	{
		Compendium c=State.instance().getWorld().getCompendium();
		
		return("Replaces text with a link to the specified modifiers's entry in the '"+c.getCodex("modifiers").getName()+"' codex of the '"+c.getName()+"'.");
	}

	@Override
	public String getMatch()
	{
		return("(MODIFIER:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)");
	}

	@Override
	public String getHRMatch()
	{
		return("MODIFIER:MODIFIER.ID");
	}
	
	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public String postProcess(String command, Character actor, CommandProperties props)
    {
		Pattern      p =getPattern(       );
	    Matcher      m =p.matcher (command);
	    StringBuffer sb=new StringBuffer ();
	    JSONObject   o;
	    
	    while(m.find())
	    {
	    	o=State.instance().getWorld().getModifierDefinitions().getModifierDef(m.group(2));
	    	
	    	if(o != null && o.has("id") && o.has("name"))
	    	{
	    		try
	    		{
			        m.appendReplacement(
			        	sb, 
			        	"<a onclick=\"game.compendium.show('modifiers','"+
			        	o.getString("id"  )+
			        	"');\">Modifier: " +
			        	o.getString("name")+
			        	"</a>"
			        );
	    		}
	    		catch(Exception e)
	    		{
	    			;
	    		}
	    	}
	    }
	    
		return(sb.toString());
    }
}
