Namespace.declare("net.lugdunon.world.defaults.compendium.craftingRecipes");
Namespace.newClass("net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.craftingRecipes.Codex,"init",[initData]);
	
	this.initialRender=false;
	this.history      =[];
	this.l;
	this.s;
	this.em           =false;
	
	game.client.getCommand("COMPENDIUM.COMMAND.PLAYER.LEARN.RECIPE").addListener(this);
	
	game.registerRenderingHook("net.lugdunon.ui.inventory.sub.CraftingGrid",this);
	
	game.client.getCommand("CORE.COMMAND.GET.PROPERTY").callDirectly(this,false,"default.compendium.crafting.recipe.easy.mode.enable");
	
	return(this);
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.propertyReceived=function(asServer,propertyKey,propertyValue)
{
	this.em=(propertyValue=="true");
};

/////////////

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.decorateIcon=function(context)
{
	var w;
	var l=game.player.getProperty(["net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"]);

	context.lineWidth  =2;
	context.font       ="bold 12px sans-serif";
	context.strokeStyle="#000";
	
	if(l != null)
	{
		context.fillStyle="#0F0";
		w                =context.measureText(l.length).width;

		context.strokeText(l.length,42-(w+4),13);
		context.fillText  (l.length,42-(w+4),13);
	}
	
	if(this.data && this.data.recipes)
	{
		context.fillStyle="#FFF";
		w                =context.measureText(this.data.recipes.length).width;

		context.strokeText(this.data.recipes.length,42-(w+4),38);
		context.fillText  (this.data.recipes.length,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.recipeLearned=function(recipeId)
{
	if(game.compendium.getActiveCodex() == this)
	{
		this.parent.find(".craftingRecipesList").find("[recipeid='"+recipeId+"'").removeClass("disabled");
	}
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.render=function(craftingGrid)
{
	if(!this.initialRender)
	{
		this.initialRender=true;
		game.getCurrentGameState().addInventoryUpdateListener("net.lugdunon.world.defaults.compendium.craftingRecipes.Codex",this);
	}
	
	this.l=$("<div class='codexCraftingLink'></div>");
	this.s=$("<div class='codexCraftingShow'></div>");

	craftingGrid.cr.append(this.l);
	craftingGrid.cr.append(this.s);
	
	this.l.on(
		"click",
		{craftingGrid:craftingGrid},
		function(e)
		{
			if(!$(this).hasClass("codexCraftingDisabled"))
			{
				game.console.focus("/ooc RECIPE:"+e.data.craftingGrid.getCraftingResult());
			}
		}
	);
	
	game.onShowHideLabel(
		this.l,
		"Link Recipe to Chat",
		function(e)
		{
			var offsetBase=$(this).offset();
			
			return(
				{
					top :offsetBase.top-18-4,
					left:offsetBase.left
				}
			);
		}
	);
	
	this.s.on(
		"click",
		{craftingGrid:craftingGrid},
		function(e)
		{
			if(!$(this).hasClass("codexCraftingDisabled"))
			{
				game.getCurrentGameState().getInventoryDialog().hide();
				game.compendium.show("crafting.recipes",e.data.craftingGrid.getCraftingResult());
			}
		}
	);
	
	game.onShowHideLabel(
		this.s,
		"View Recipe in The '"+game.compendium.name+"'",
		function(e)
		{
			var offsetBase=$(this).offset();
			
			return(
				{
					top :offsetBase.top-18-4,
					left:offsetBase.left
				}
			);
		}
	);
	
	this.craftingResultUpdated();
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.craftingResultUpdated=function(character,recipeId,craftedItemId,craftedItemCount,craftedStamina)
{
	var id=game.getCurrentGameState().getInventoryDialog();

	if(id != null && id.getSubpanel() != null && Namespace.instanceOf(id.getSubpanel(),"net.lugdunon.ui.inventory.sub.CraftingGrid"))
	{
		if(recipeId == null || recipeId == "")
		{
			this.l.addClass("codexCraftingDisabled");
			this.s.addClass("codexCraftingDisabled");
		}
		else
		{
			this.l.removeClass("codexCraftingDisabled");
			this.s.removeClass("codexCraftingDisabled");
		}
	}
	
	try
	{
		game.compendium.requestIconUpdate(this);
	}
	catch(e)
	{
		;
	}
};

/////////////

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.fetched=function()
{
	this.parent.html(Namespace.requireHTML(this.classId));
	
	var i;
	var e;
	var f;
	var l=game.player.getProperty(["net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"]);

	this.history.length=0;
	
	for(i=0;i<this.data.recipes.length;i++)
	{
		e=this.data.recipes[i];
		f=net.lugdunon.ui.icon.IconSelectorDialog.createIconDOM(
			this,
			e            ==  null?"No Recipe":game.items[e.result.itemId].getName(),
			false,
			0,0,32,32,
			e,
			true
		);
		
		f.attr("recipeid",e.recipeId);
		
		if(
			(!this.em) &&
			(l == null || !l.contains(e.recipeId))
		)
		{
			f.addClass("disabled");
		}
		
		this.parent.find(".craftingRecipesList").append(f);
	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.craftingRecipes.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.highlight=function(recipeId,addToHistory)
{
	var context=this;
	var l      =game.player.getProperty(["net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"]);
	var r      =-1;
	var i;
		
	for(i=0;i<this.data.recipes.length;i++)
	{
		if(this.data.recipes[i].recipeId == recipeId)
		{
			r=i;
			break;
		}
	}
	
	if(r > -1)
	{
		this.parent.find(".craftingRecipesList").animate(
			{
				scrollTop: Math.floor(r/10)*64
			}, 
			1000
		);
	}

	if(
		(!this.em) &&
		(l == null || !l.contains(e.recipeId))
	)
	{
		this.parent.find(".craftingRecipesList").find("[recipeid='"+recipeId+"'").trigger("click");
	}
	else if(r > -1)
	{
		this.setData(this.data.recipes[r],null,addToHistory);
	}
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.getHistoryItem=function(recipe)
{
	var h={recipe:recipe,dom:$("<div class='inventoryItem'><canvas width='42' height='42'/></div>")};

	this.drawItem(
		h.dom.find("canvas")[0].getContext("2d"),
		recipe.result
	);
	
	h.dom.on(
		"click",
		{context:this,recipeId:recipe.result.itemId},
		function(e)
		{
			e.data.context.highlight.call(e.data.context,e.data.recipeId);
		}
	);
	
	game.onShowHideLabel(
		h.dom,
		function(e)
		{
			return(recipe.result==null?"No Item":game.items[recipe.result.itemId].getName());
		},
		function(e)
		{
			var offsetBase=$(this).offset();
			
			return(
				{
					top :offsetBase.top-18-4,
					left:offsetBase.left
				}
			);
		}
	);
	
	return(h);
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.setData=function(recipe,iconEl,addToHistory)
{
	var i;
	var l;
	var e;
	var t;
	var h=$("<div class='craftingHistoryContainer'><div class='craftingHCBG'>&#xF017;</div></div>");
	var c=$("<div class='craftingContainer'><div class='craftingBG'></div></div>");
	var a=$("<div class='craftingArrowCost'><div class='craftingArrow'>&#xF138;</div><div class='craftingCost'></div></div>");
	var r=$("<div class='craftingResult'><div class='inventoryItem'><canvas width='42' height='42'/></div></div>");
	var s=r.find(".inventoryItem");
	var b=c.find(".craftingBG");

	//HISTORY STUFF
	{
		if(addToHistory)
		{
			this.history.push(this.getHistoryItem(recipe));
			
			while(this.history.length > 9)
			{
				this.history.shift();
			}
		}
		else
		{
			t=0;
			
			for(i=0;i<this.history.length;i++)
			{
				if(this.history[i].recipe.recipeId == recipe.recipeId)
				{
					t=i;
					break;
				}
			}
			
			if(t==0)
			{
				this.history.length=0;
				this.history.push(this.getHistoryItem(recipe));
			}
			else
			{
				this.history=this.history.slice(0,t+1);
			}
		}
		
		for(i=0;i<this.history.length;i++)
		{
			h.append(this.history[i].dom);
		}
	}
	
	b.css("left","0px");
	b.css("top", "0px");
	
	if(recipe.recipe.length <= 9)
	{
		l=9;

		h.css("margin-top",   "0px");
		a.css("margin-top",  "44px");
		
		b.css("width",      "132px");
		b.css("height",     "132px");
		c.css("width",      "132px");
		c.css("height",     "132px");

		c.css("margin-left", ((this.parent.find(".craftingRecipesDemo").width ()/2)-( 74+132))     +"px");
		
		this.parent.find(".craftingRecipesContainer").addClass   ("cc3x3");
		this.parent.find(".craftingRecipesContainer").removeClass("cc4x4");
		this.parent.find(".craftingRecipesContainer").removeClass("cc5x5");
	}
	else if(recipe.recipe.length <=16)
	{
		l=16;

		h.css("margin-top",  "22px");
		a.css("margin-top",  "66px");

		b.css("width",      "176px");
		b.css("height",     "176px");
		c.css("width",      "176px");
		c.css("height",     "176px");
		
		c.css("margin-left", ((this.parent.find(".craftingRecipesDemo").width ()/2)-( 96+132))     +"px");
		
		this.parent.find(".craftingRecipesContainer").addClass   ("cc4x4");
		this.parent.find(".craftingRecipesContainer").removeClass("cc3x3");
		this.parent.find(".craftingRecipesContainer").removeClass("cc5x5");
	}
	else
	{
		l=25;

		h.css("margin-top",  "44px");
		a.css("margin-top",  "88px");

		b.css("width",      "220px");
		b.css("height",     "220px");
		c.css("width",      "220px");
		c.css("height",     "220px");

		c.css("margin-left", ((this.parent.find(".craftingRecipesDemo").width ()/2)-(118+132))     +"px");
		
		this.parent.find(".craftingRecipesContainer").addClass   ("cc5x5");
		this.parent.find(".craftingRecipesContainer").removeClass("cc3x3");
		this.parent.find(".craftingRecipesContainer").removeClass("cc4x4");
	}
	
	//SET BG IMAGE
	b.css("background-image","url(./assets/icons/"+game.craftingDisciplines[recipe.craftType].icon+".svg)");

	this.parent.find(".craftingRecipesDemo").html  (h);
	this.parent.find(".craftingRecipesDemo").append(c);
	this.parent.find(".craftingRecipesDemo").append(a);
	this.parent.find(".craftingRecipesDemo").append(r);
	
	a.find(".craftingCost").html("STA<br/>"+recipe.result.stamina);
	
	//RESULT
	{
		this.drawItem(
			s.children(":last")[0].getContext("2d"),
			recipe.result
		);
		
		var ccl=$("<div class='codexCraftingLink'></div>");

		r.append(ccl);
		
		ccl.on(
			"click",
			{recipe:recipe},
			function(e)
			{
				game.compendium.hide();
				game.console.focus("/ooc RECIPE:"+e.data.recipe.recipeId);
			}
		);
		
		game.onShowHideLabel(
			ccl,
			"Link Recipe to Chat",
			function(e)
			{
				var offsetBase=$(this).offset();
				
				return(
					{
						top :offsetBase.top-18-4,
						left:offsetBase.left
					}
				);
			}
		);
		
//		s.on(
//			"click",
//			{context:this,recipeId:recipe.result.itemId},
//			function(e)
//			{
//				e.data.context.highlight.call(e.data.context,e.data.recipeId);
//			}
//		);
		
		game.onShowHideLabel(
			s,
			function(e)
			{
				return(recipe.result==null?"No Item":game.items[recipe.result.itemId].getName());
			},
			function(e)
			{
				var offsetBase=$(this).offset();
				
				return(
					{
						top :offsetBase.top-18-4,
						left:offsetBase.left
					}
				);
			}
		);
	}
	
	for(i=0;i<l;i++)
	{
		r=(recipe.recipe.length > i)?(recipe.recipe[i]):(null);
		e=$("<div class='inventoryItem' data-index='"+i+"'><canvas width='42' height='42'/></div>");
		
		c.append(e);
		e.attr("data-recipeName",(r==null?"No Item":game.items[r.itemId].getName()));
		
		this.drawItem(
			e.children(":last")[0].getContext("2d"),
			r
		);
		
		if(r != null)
		{
			this.setConsumed(e,r.consumed);
			
			e.on(
				"click",
				{context:this,recipeId:r.itemId},
				function(e)
				{
					e.data.context.highlight.call(e.data.context,e.data.recipeId,true);
				}
			);
		}
		
		game.onShowHideLabel(
			e,
			function(e)
			{
				return($(this).attr("data-recipeName"));
			},
			function(e)
			{
				var offsetBase=$(this).offset();
				
				return(
					{
						top :offsetBase.top-18-4,
						left:offsetBase.left
					}
				);
			}
		);
	}
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.setConsumed=function(el,consumed)
{
	if(consumed === false)
	{
		el.addClass   ("inventoryItemSelected");
	}
	else
	{
		el.removeClass("inventoryItemSelected");
	}
};

net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.prototype.drawItem=function(context,item)
{
	context.clearRect(0,0,42,42);

	if(item != null)
	{
		context.drawImage(game.items[item.itemId].getIcon(),0,0,32,32,5,5,32,32);
		
		if(item.stackSize != null && item.stackSize > 1)
		{
			var w;
			
			context.fillStyle  ="#FFF";
			context.strokeStyle="#000";
			context.lineWidth  =2;
			context.font       ="bold 12px sans-serif";
			
			w=context.measureText(item.stackSize).width;

			context.strokeText(item.stackSize,42-(w+4),36);
			context.fillText  (item.stackSize,42-(w+4),36);
		}
	}
};