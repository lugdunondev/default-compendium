Namespace.declare("net.lugdunon.world.defaults.compendium.craftingRecipes.command");

Namespace.newClass("net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand","net.lugdunon.command.core.EventProducerCommand");

// //

net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand.prototype.opInit=function(initData)
{

};

net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand.prototype.getCommandLength=function()
{
    return(0);
};

net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand.prototype.buildCommand=function(dataView)
{

};

net.lugdunon.world.defaults.compendium.craftingRecipes.command.PlayerLearnCraftingRecipeCommand.prototype.commandResponse=function(res)
{
	var r=res.readString();
	var p=game.player.getProperty(["net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"]);
	
	if(p == null)
	{
		p=[];
		game.player.setProperty(["net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"],p);
	}
	
	p.push(r);

	this.callListeners("recipeLearned",[r]);
	
	game.console.log("You learned how to craft '"+game.items[res.readString()].getName()+"'!","#0F0","craftingRecipeLearned",true);
};