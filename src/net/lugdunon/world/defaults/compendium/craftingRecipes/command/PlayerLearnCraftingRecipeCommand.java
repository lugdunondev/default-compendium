package net.lugdunon.world.defaults.compendium.craftingRecipes.command;

import java.io.IOException;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.recipe.Recipe;

import org.json.JSONArray;
import org.json.JSONException;

public class PlayerLearnCraftingRecipeCommand extends Command implements IServerInvokedCommand
{
	protected Response        iRes;
	
	////
	
	@Override
	public String getCommandId()
	{
	    return("COMPENDIUM.COMMAND.PLAYER.LEARN.RECIPE");
	}

	@Override
	public String getName()
	{
		return("Player Learn Recipe");
	}

	@Override
	public String getDescription()
	{
		return("Handles a player learning a crafting recipe.");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
	public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		PlayerCharacter pc=props.getPlayerCharacter("character");
		Recipe          r =props.getRecipe         ("recipe"   );
		
		try
        {  
			Response  oRes;
			JSONArray a   =(JSONArray) pc.getProp("net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.learnedRecipes");
	        
	        if(a == null)
	        {
	        	a=new JSONArray();
	        }
	        
	        for(int i=0;i<a.length();i++)
	        {
	        	if(r.getRecipeId().equals(a.getString(i)))
	        	{
	        		return;
	        	}
	        }
	        
	        a.put(r.getRecipeId());

	        pc.setProp(new String[]{"net.lugdunon.world.defaults.compendium.craftingRecipes.Codex","learnedRecipes"},a);
	        
			iRes=initializeInternalResponse();
			
			iRes.out.writeUTF(r.getRecipeId()          );
			iRes.out.writeUTF(r.getResult().getItemId());
			
			oRes=initializeResponse();
			oRes.out.write(iRes.bytes());
			
			pc.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
	}

	@Override
    public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
    {
	    //DO NOTHING
    }
}