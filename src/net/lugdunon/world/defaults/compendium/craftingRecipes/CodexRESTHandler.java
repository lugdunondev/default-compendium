package net.lugdunon.world.defaults.compendium.craftingRecipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.state.State;
import net.lugdunon.state.State.IPlayerCharacterConnectionListener;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;
import net.lugdunon.state.metric.IMetricListener;
import net.lugdunon.state.recipe.Recipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler implements IPlayerCharacterConnectionListener
{
//	private static Logger                                       LOGGER=LoggerFactory.getLogger(CodexRESTHandler.class);
	private static Map<String,PlayerCraftedItemMetricListener> PCIMLS =new HashMap<String,PlayerCraftedItemMetricListener>();
	
	public CodexRESTHandler()
	{
		State.instance().addPlayerCharacterConnectionListener(this);
		super.setStartsWith(true);
	}
	
	@Override
    public String getName()
    {
	    return("Crafting Recipes");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's available crafting recipes.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		List<Recipe>       recipes=new ArrayList<Recipe>(State.instance().getWorld().getRecipes().listRecipes());
		JSONObject         o      =new JSONObject();
		JSONArray          a      =new JSONArray ();
		Collection<String> c      =getLearnedRecipes(context);
		
		o.put("recipes",a);
		
		Collections.sort(recipes);
		
		for(Recipe recipe:recipes)
		{
			if(
				State.instance().getWorld().getWorldConfigProperty("default.compendium.crafting.recipe.easy.mode.enable", false) ||
				(c == null || c.contains(recipe.getRecipeId()))
			)
			{
				a.put(recipe.toJSONObject());
			}
		}
		
	    return(o);
    }
	
	private Collection<String> getLearnedRecipes(String context) throws JSONException
	{
		List<String>    l =null;
		PlayerCharacter p =State.instance().findCharacter(context.substring(getContext().length()).replace("/", ""));	
		
		if(p != null)
		{
			JSONArray lr=(JSONArray) p.getProp("net.lugdunon.world.defaults.compendium.craftingRecipes.Codex.learnedRecipes");

			l=new ArrayList<String>();
			
			if(lr != null)
			{
				for(int i=0;i<lr.length();i++)
				{
					l.add(lr.getString(i));
				}
			}
		}

		return(l);
	}

	@Override
    public void characterConnected(PlayerCharacter pc)
    {
		PCIMLS.put(pc.getName(),new PlayerCraftedItemMetricListener(pc));	 
		pc.getMetricsManager().addListener("CORE.METRIC.ITEM.CRAFTED",PCIMLS.get(pc.getName()));
    }

	@Override
    public void characterDisconnected(PlayerCharacter pc)
    {
		if(PCIMLS.containsKey(pc.getName()))
		{
			pc.getMetricsManager().removeListener("CORE.METRIC.ITEM.CRAFTED",PCIMLS.remove(pc.getName()));
		}
    }

	private static class PlayerCraftedItemMetricListener implements IMetricListener
	{
		PlayerCharacter pc;
		
		public PlayerCraftedItemMetricListener(PlayerCharacter pc)
		{
			this.pc=pc;
		}
		
		@Override
	    public void metricUpdated(String metricType, Object metricValue, Object transientValue)
	    {
			try
			{
				CommandProperties props=new CommandProperties();

				props.setPlayerCharacter("character",pc);
				props.setRecipe         ("recipe",  State.instance().getWorld().getRecipes().getRecipe(((JSONObject) transientValue).getString("recipeId")));
				
			    State.instance().getGame().addIncomingRequest(
			    	"COMPENDIUM.COMMAND.PLAYER.LEARN.RECIPE",
					props
			    );
			}
            catch (CommandNotSupportedException e)
            {
	            e.printStackTrace();
            }
            catch (IsNotServerInvokedCommand e)
            {
	            e.printStackTrace();
            }
            catch (JSONException e)
            {
	            e.printStackTrace();
            }
	    }
	}
}