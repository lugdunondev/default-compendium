package net.lugdunon.world.defaults.compendium.instantiatedPlayerItems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;
import net.lugdunon.state.item.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Instantiated Player Items");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's items that can be instantiated and that are available to players.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		List<Item>               items       =new ArrayList<Item>(State.instance().getWorld().getItemDefinitions().listItemDefinitions());
		Map<String,List<String>> types       =new HashMap<String,List<String>>();
		List<String>             spellTypes  =new ArrayList<String>();
		List<String>             spellSchools=new ArrayList<String>();
		JSONObject               o           =new JSONObject();
		JSONArray                i           =new JSONArray ();
		JSONObject               it          =new JSONObject();
		JSONArray                st          =new JSONArray ();
		JSONArray                ss          =new JSONArray ();
		
		o.put("items",       i );
		o.put("itemTypes",   it);
		o.put("spellTypes",  st);
		o.put("spellSchools",ss);
		
		Collections.sort(items);
		
		for(Item item:items)
		{
			if(!item.isNonInstanced() && !item.isNpcOnly() && !"ACTION.PLAY.HAND".equals(item.getItemId()))
			{
				i.put(item.toJSONObject());
				
//				if(item.getItemType() == null)
//				{
//					System.out.println(item.getItemId());
//				}
				
				if(item.getItemType() != null && !types.containsKey(item.getItemType()))
				{
					types.put(item.getItemType(),new ArrayList<String>());
					it   .put(item.getItemType(),new JSONArray        ());
				}
				
				if(item.getItemType() != null && item.getItemSubType() != null && !types.get(item.getItemType()).contains(item.getItemSubType()))
				{
					types.get         (item.getItemType()).add(item.getItemSubType());
					it   .getJSONArray(item.getItemType()).put(item.getItemSubType());
				}
				
				if(item.getItemSpellType() != null && !spellTypes.contains(item.getItemSpellType()))
				{
					st.put(item.getItemSpellType());
				}
				
				if(item.getItemSpellSchool() != null && !spellSchools.contains(item.getItemSpellSchool()))
				{
					ss.put(item.getItemSpellSchool());
				}
			}
		}
		
	    return(o);
    }
}
