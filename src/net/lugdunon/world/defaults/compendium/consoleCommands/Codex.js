Namespace.declare("net.lugdunon.world.defaults.compendium.consoleCommands");
Namespace.newClass("net.lugdunon.world.defaults.compendium.consoleCommands.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.consoleCommands.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.consoleCommands.Codex,"init",[initData]);
	this.data=null;
	
	return(this);
};

net.lugdunon.world.defaults.compendium.consoleCommands.Codex.prototype.decorateIcon=function(context)
{
	if(this.data && this.data.commands)
	{
		var w;
		
		context.fillStyle  ="#FFF";
		context.strokeStyle="#000";
		context.lineWidth  =2;
		context.font       ="bold 12px sans-serif";
		
		w                  =context.measureText(this.data.commands.length).width;

		context.strokeText(this.data.commands.length,42-(w+4),38);
		context.fillText  (this.data.commands.length,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.consoleCommands.Codex.prototype.fetched=function()
{
	var i;

	if(game.gmFlag)
	{
		this.parent.append("<div class='consoleCommandListHeader'>GM Commands</div>");
		
		for(i=0;i<this.data.commands.length;i++)
		{
			if(this.data.commands[i].gmOnly === true)
			{
				this.renderCommand(this.data.commands[i]);
			}
		}
		
		this.parent.append("<div class='consoleCommandListHeader'>Player Commands</div>");
	}
	
	for(i=0;i<this.data.commands.length;i++)
	{
		if(!this.data.commands[i].gmOnly)
		{
			this.renderCommand(this.data.commands[i]);
		}
	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.consoleCommands.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.consoleCommands.Codex.prototype.renderCommand=function(command)
{
	var i;
	var c;
	var a="";
	var p="";
	
	if(command.aliases && command.aliases.length > 0)
	{
		for(i=0;i<command.aliases.length;i++)
		{
			a+="<div class='consoleCommandListItemSubDetails'>/"+command.aliases[i]+"</div>";
		}
	}
	else
	{
		a+="<div class='consoleCommandListItemSubDetails'>None</div>";
	}
	
	if(command.parameters && command.parameters.length > 0)
	{
		for(i=0;i<command.parameters.length;i++)
		{
			p+="<div class='consoleCommandListItemSubDetails'>"+
			   ((command.parameters[i].parameterMap.indexOf("/") == 0)?(""):("/"+command.command))+
			   ((command.parameters[i].parameterMap == "")?(""):(" "+command.parameters[i].parameterMap))+" - "+
			   command.parameters[i].description+"</div>";
		}
	}
	else
	{
		p+="<div class='consoleCommandListItemSubDetails'>/"+command.command+" - "+command.description+"</div>";
	}
	
	c=$(
		"<div class='consoleCommandListItem'>"+
			"<div class='consoleCommandListItemToggle'></div>"+
			"<div class='consoleCommandListItemName'>"+command.name+"</div>"+
		    "<div class='consoleCommandListItemDetails'>Command:</div>"+
		    "<div class='consoleCommandListItemSubDetails'>/"+command.command+"</div>"+
			"<div class='consoleCommandListItemDetails'>Aliases:</div>"+
			a+
			"<div class='consoleCommandListItemDetails'>Examples:</div>"+
			p+
		"</div>"
	);
	
	c.find(".consoleCommandListItemToggle").on(
		"click",
		{context:c},
		function(e)
		{
			if(e.data.context.hasClass("consoleCommandListItemExpanded"))
			{
				e.data.context.removeClass("consoleCommandListItemExpanded");
			}
			else
			{
				e.data.context.addClass("consoleCommandListItemExpanded");
			}
		}
	);
	
	this.parent.append(c);
};

net.lugdunon.world.defaults.compendium.consoleCommands.Codex.prototype.highlight=function(data)
{
	;
};