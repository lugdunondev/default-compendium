package net.lugdunon.world.defaults.compendium.consoleCommands;

import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.command.core.console.ConsoleFiredCommand;
import net.lugdunon.command.core.console.ConsoleFiredCommand.ConsoleCommandParameter;
import net.lugdunon.command.core.console.gm.GmOnlyConsoleFiredCommand;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Console Commands");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's available console commands.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		List<String>                   cfCommandIds=State.instance().getInterpreter().listConsoleFiredCommands();
		SortedSet<ConsoleFiredCommand> cfCommands  =new TreeSet<ConsoleFiredCommand>();
		JSONObject                      o          =new JSONObject();
		JSONArray                       a          =new JSONArray ();
		JSONObject                      p;
		JSONObject                      q;
		JSONArray                       b;
		
		o.put("commands",a);
		
		for(String cfCommandId:cfCommandIds)
		{
			try
			{
	        	cfCommands.add((ConsoleFiredCommand) State.instance().getInterpreter().getCommand(cfCommandId));
			}
			catch(CommandNotSupportedException e)
			{
				e.printStackTrace();
			}
		}
		
		for(ConsoleFiredCommand cfCommand:cfCommands)
		{
    		String[]                  aliases   =cfCommand.getConsoleOpCodeAliases    ();
    		ConsoleCommandParameter[] parameters=cfCommand.getConsoleCommandParameters();
        	
        	p=new JSONObject();
        	
        	a.put(p); 
        	
        	p.put("name",       cfCommand.getName              ());
        	p.put("description",cfCommand.getDescription       ());
        	p.put("command",    cfCommand.getConsoleOpCodeAlias());
        	
        	if(cfCommand instanceof GmOnlyConsoleFiredCommand)
        	{
            	p.put("gmOnly",true);
        	}
        	
        	if(aliases != null)
        	{
        		Arrays.sort(aliases);
        		
            	b=new JSONArray();
        		p.put("aliases",b);
            	
            	for(String alias:aliases)
            	{
            		b.put(alias);
            	}
        	}
        	
        	if(parameters != null)
        	{
            	b=new JSONArray();
        		p.put("parameters",b);
            	
            	for(ConsoleCommandParameter parameter:parameters)
            	{
            		q=new JSONObject();

            		q.put("parameterMap",parameter.getParameterMap());
            		q.put("description", parameter.getDescription ());
            		
            		b.put(q);
            	}
        	}
		}
		
	    return(o);
    }
}