Namespace.declare("net.lugdunon.world.defaults.compendium.environment");
Namespace.newClass("net.lugdunon.world.defaults.compendium.environment.Codex","net.lugdunon.state.compendium.Codex");

/*
,
		{
			id            :"environment",
			icon          :"SPELL_SNOW",
			name          :"The Taranis Codex",
			description   :"Provides a complete accounting of this server's environment.",
			implementation:
			{
				ui        :"net.lugdunon.world.defaults.compendium.environment.Codex",
				rest      :"net.lugdunon.world.defaults.compendium.environment.CodexRESTHandler"
			}
		}
		
		
		
	manifest   :{
		IMG:[
			{key:"ENV_SEASONS",        def:"net.lugdunon.world.defaults.compendium.environment.assets.Seasons"       },
			{key:"ENV_SEASONS_OVERLAY",def:"net.lugdunon.world.defaults.compendium.environment.assets.SeasonsOverlay"},
			{key:"ENV_DATE_INDICATOR", def:"net.lugdunon.world.defaults.compendium.environment.assets.DateIndicator" }
		]
	},
 */

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.init=function(initData)
{
//	initData.description+="</div><div class='ttlineItem'>&nbsp;</div><div class='ttlineItem' style='color:#0F0'>Clicking on an item links it in chat.</div>";
//	
//	if(game.gmFlag)
//	{
//		initData.description+=
//			"<div class='ttlineItem'>&nbsp;</div><div class='ttlineItem' style='color:#0F0'>Clicking with the "+
//			"secondary mouse button will auto assign an instance (or stack) of the item to the current player's "+
//			"target, or to the player themselves if no target is selected.</div>"+
//			"<div class='ttlineItem'>&nbsp;</div><div class='ttlineItem' style='color:#0F0'>Clicking with the "+
//			"middle mouse button will open the chat console with a /give command prepopulated with the item.</div>";
//	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.environment.Codex,"init",[initData]);
	
	return(this);
};

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.decorateIcon=function(context)
{
//	if(this.data && this.data.items)
//	{
//		var w;
//		
//		context.fillStyle  ="#FFF";
//		context.strokeStyle="#000";
//		context.lineWidth  =2;
//		context.font       ="bold 12px sans-serif";
//		
//		w                  =context.measureText(this.data.items.length).width;
//
//		context.strokeText(this.data.items.length,42-(w+4),38);
//		context.fillText  (this.data.items.length,42-(w+4),38);
//	}
};

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.secondaryFetched=function(data)
{
	//this.secondaryFetch({mode:"elevation"});
	
	console.log("WEI");
};

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.fetched=function()
{
	console.log(this.data);
	
	this.secondaryFetch({mode:"elevation"});
	
//	var i;
//	var e;
//	var f;
//
//	for(i=0;i<this.data.items.length;i++)
//	{
//		e=this.data.items[i];
//		f=net.lugdunon.ui.icon.IconSelectorDialog.createIconDOM(
//			this,
//			e            ==  null?"No Item":game.items[e.itemId].getName(),
//			false,
//			0,0,32,32,
//			e,
//			true
//		);
//		
//		f.attr("itemid",e.itemId);
//		
//		this.parent.append(f);
//	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.environment.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.highlight=function(itemId)
{
//	this.parent.animate(
//		{
//			scrollTop: $("[itemid='"+itemId+"']").position().top
//		}, 
//		1000
//	);
};

net.lugdunon.world.defaults.compendium.environment.Codex.prototype.setData=function(data,iconEl,button,metaKey)
{
//	switch(button)
//	{
//		//LEFT
//		case net.lugdunon.input.Input.CLICK_TYPE_PRIMARY:
//		{
//			//LINK
//			game.compendium.hide ();
//			game.console.focus("/ooc ITEM:"+data.itemId);
//			
//			break;
//		}
//		//RIGHT
//		case net.lugdunon.input.Input.CLICK_TYPE_SECONDARY:
//		{
//			// GIVE
//			if(game.gmFlag)
//			{
//				game.console.exec(
//					"/give "+
//					((game.player.target != null && Namespace.instanceOf(game.player.target,"net.lugdunon.character.PlayerCharacter"))?("%t "):(""))+
//					data.itemId+
//					((data.stackable)?(" "+net.lugdunon.item.Item.MAX_STACK_SIZE):(""))
//				);
//			}
//			break;
//		}
//		//MIDDLE
//		case net.lugdunon.input.Input.CLICK_TYPE_TERTIARY:
//		{
//			// FOCUS
//			if(game.gmFlag)
//			{
//				game.compendium.hide ();
//				game.console   .focus(
//					"/give "+
//					((game.player.target != null && Namespace.instanceOf(game.player.target,"net.lugdunon.character.PlayerCharacter"))?("%t "):(""))+
//					data.itemId+
//					((data.stackable)?(" "+net.lugdunon.item.Item.MAX_STACK_SIZE):(""))
//				);
//			}
//			
//			break;
//		}
//	}
};