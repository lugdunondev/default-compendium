package net.lugdunon.world.defaults.compendium.environment;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.state.State;
import net.lugdunon.state.SubsystemBase;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;
import net.lugdunon.world.environment.IEnvironment;
import net.lugdunon.world.instance.Instance;

import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Environment");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's environment.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		IEnvironment environment=State.instance().getWorld().getSubsystems().getEnvironment();
		JSONObject   o          =new JSONObject();
		String       mode       =request.getParameter("mode");
		
		System.out.println(mode);
		
		try
        {
			if(mode == null)
			{
				//WRITE OUT TERRAIN STUFF (IMAGES)
		        o.put(
		        	"layerImage",
		        	State.instance().getWorld().getOverworldInstance().getTerrain().getLayerImageData()
		        );
	
				//WRITE OUT CLIMATE INFO
				o.put(
					"climateInformationImpl",
					environment.getClimateInformation().getClass()
				);
				o.put(
					"climateInformation",
					environment.getClimateInformation().toJSONObject()
				);
				
				//WRITE OUT WEATHER FORECAST
				o.put(
					"weatherForecastImpl",
					environment.getCurrentWorldwideWeatherForecast().getClass()
				);
				o.put(
					"weatherForecast",
					environment.getCurrentWorldwideWeatherForecast().toJSONObject()
				);
			}
			else if("elevation".equals(mode))
			{
		        o.put(
		        	"elevationImage",
		        	State.instance().getWorld().getOverworldInstance().getTerrain().getElevationImageData()
		        );
			}
			else if("biome".equals(mode))
			{
		        o.put(
		        	"elevationImage",
		        	State.instance().getWorld().getOverworldInstance().getTerrain().getBiomeImageData()
		        );
			}
        }
        catch (Exception e)
        {
	        e.printStackTrace();
        }
		
		return(o);
    }
}
