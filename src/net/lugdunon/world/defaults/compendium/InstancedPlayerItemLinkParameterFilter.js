Namespace.declare("net.lugdunon.world.defaults.compendium");

Namespace.newClass("net.lugdunon.world.defaults.compendium.InstancedPlayerItemLinkParameterFilter","net.lugdunon.command.core.console.parameters.IParameterFilter");

// //

net.lugdunon.world.defaults.compendium.InstancedPlayerItemLinkParameterFilter.prototype.init=function(initData)
{
	this.matcher    =/(ITEM:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)/gm;
	this.replacement=function(match, item, itemId)
	{
		return(
        	"<a onclick=\"game.compendium.show('instantiated.player.items','"+
        	itemId+
        	"');\">Item: "+
        	game.items[itemId].name+
        	"</a>"
		);
	};

	return(this);
};

net.lugdunon.world.defaults.compendium.InstancedPlayerItemLinkParameterFilter.prototype.process=function(text, actor, props)
{
	return(
		text.replace(
			this.matcher,
			this.replacement
		)
	);
};