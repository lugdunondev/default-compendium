Namespace.declare("net.lugdunon.world.defaults.compendium");

Namespace.newClass("net.lugdunon.world.defaults.compendium.CraftingRecipeLinkParameterFilter","net.lugdunon.command.core.console.parameters.IParameterFilter");

// //

net.lugdunon.world.defaults.compendium.CraftingRecipeLinkParameterFilter.prototype.init=function(initData)
{
	this.matcher    =/(RECIPE:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)/gm;
	this.replacement=function(match, rec, recId)
	{
		if(game.recipes[recId])
		{
			return(
		    	"<a class='recipe' onclick=\"game.compendium.show('crafting.recipes','"+
		    	recId+
		    	"');\">Recipe: "+
		    	game.items[
			    	game.recipes[
			    	    recId
			    	].result.itemId
		    	].name+
		    	"</a>"
			);
		}
		else
		{
			return(match);
		}
	};

	return(this);
};

net.lugdunon.world.defaults.compendium.CraftingRecipeLinkParameterFilter.prototype.process=function(text, actor, props)
{
	return(
		text.replace(
			this.matcher,
			this.replacement
		)
	);
};