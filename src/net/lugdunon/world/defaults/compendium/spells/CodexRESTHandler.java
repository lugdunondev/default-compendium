package net.lugdunon.world.defaults.compendium.spells;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;
import net.lugdunon.state.item.Item;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Spells");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's spells.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		List<Item> items=new ArrayList<Item>(State.instance().getWorld().getItemDefinitions().listItemDefinitions());
		JSONObject o    =new JSONObject();
		JSONArray  a    =new JSONArray ();
		
		o.put("spells",a);
		
		Collections.sort(items);
		
		for(Item item:items)
		{
			if(item.isSpell() && !item.isNpcOnly())
			{
				a.put(item.toJSONObject());
			}
		}
		
	    return(o);
    }
}
