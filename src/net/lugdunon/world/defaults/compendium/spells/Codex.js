Namespace.declare("net.lugdunon.world.defaults.compendium.spells");
Namespace.newClass("net.lugdunon.world.defaults.compendium.spells.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.spells.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.spells.Codex,"init",[initData]);
	
	return(this);
};

net.lugdunon.world.defaults.compendium.spells.Codex.prototype.decorateIcon=function(context)
{
	if(this.data && this.data.spells)
	{
		var w;
		
		context.fillStyle  ="#FFF";
		context.strokeStyle="#000";
		context.lineWidth  =2;
		context.font       ="bold 12px sans-serif";
		
		w                  =context.measureText(this.data.spells.length).width;

		context.strokeText(this.data.spells.length,42-(w+4),38);
		context.fillText  (this.data.spells.length,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.spells.Codex.prototype.fetched=function()
{
	var i;
	var e;
	var f;
	
	for(i=0;i<this.data.spells.length;i++)
	{
		e=this.data.spells[i];
		f=net.lugdunon.ui.icon.IconSelectorDialog.createIconDOM(
			game.gmFlag  === true?this     :null,
			e            ==  null?"No Spell":game.items[e.itemId].getName(),
			game.items[e.itemId].getIcon(),
			0,0,32,32,
			e,
			true
		);
		
		f.attr("spellid",e.itemId);
		
		this.parent.append(f);
	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.spells.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.spells.Codex.prototype.highlight=function(spellId)
{
	this.parent.animate(
		{
			scrollTop: $("[spellid='"+spellId+"']").position().top
		}, 
		1000
	);
};

net.lugdunon.world.defaults.compendium.spells.Codex.prototype.setData=function(data)
{
	//DO NOTHING
};