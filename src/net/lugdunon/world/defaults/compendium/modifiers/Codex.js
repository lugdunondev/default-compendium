Namespace.declare("net.lugdunon.world.defaults.compendium.modifiers");
Namespace.newClass("net.lugdunon.world.defaults.compendium.modifiers.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.modifiers.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.modifiers.Codex,"init",[initData]);
	
	return(this);
};

net.lugdunon.world.defaults.compendium.modifiers.Codex.prototype.decorateIcon=function(context)
{
	if(this.data && this.data.modifiers)
	{
		var w;
		
		context.fillStyle  ="#FFF";
		context.strokeStyle="#000";
		context.lineWidth  =2;
		context.font       ="bold 12px sans-serif";
		
		w                  =context.measureText(this.data.modifiers.length).width;

		context.strokeText(this.data.modifiers.length,42-(w+4),38);
		context.fillText  (this.data.modifiers.length,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.modifiers.Codex.prototype.fetched=function()
{
	var i;
	var e;
	var f;
	
	for(i=0;i<this.data.modifiers.length;i++)
	{
		e=this.data.modifiers[i];
		f=net.lugdunon.ui.icon.IconSelectorDialog.createIconDOM(
			game.gmFlag  === true?this     :null,
			e.name,
			game.getIcon(e.iconId),
			0,0,32,32,
			e,
			true
		);
		
		f.attr("spellid",e.itemId);
		
		this.parent.append(f);
	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.modifiers.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.modifiers.Codex.prototype.highlight=function(modifierId)
{
	this.parent.animate(
		{
			scrollTop: $("[modifierid='"+modifierId+"']").position().top
		}, 
		1000
	);
};

net.lugdunon.world.defaults.compendium.modifiers.Codex.prototype.setData=function(data)
{
	//DO NOTHING
};