package net.lugdunon.world.defaults.compendium.modifiers;

import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Modifiers");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's modifiers.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		SortedSet<String> modifierIds=new TreeSet<String>();
		JSONObject        o          =new JSONObject     ();
		JSONArray         a          =new JSONArray      ();
		
		for(JSONObject modifier:State.instance().getWorld().getModifierDefinitions().listModifierDefinitions())
		{
			modifierIds.add(modifier.getString("id"));
		}
		
		o.put("modifiers",a);
		
		for(String modifierId:modifierIds)
		{
			a.put(State.instance().getWorld().getModifierDefinitions().getModifierDef(modifierId));
		}
		
	    return(o);
    }
}
