Namespace.declare("net.lugdunon.world.defaults.compendium.consoleParameterFilters");
Namespace.newClass("net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex","net.lugdunon.state.compendium.Codex");

net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex,"init",[initData]);
	
	return(this);
};

net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex.prototype.decorateIcon=function(context)
{
	if(this.data && this.data.parameterFilters)
	{
		var w;
		
		context.fillStyle  ="#FFF";
		context.strokeStyle="#000";
		context.lineWidth  =2;
		context.font       ="bold 12px sans-serif";
		
		w                  =context.measureText(this.data.parameterFilters.length).width;

		context.strokeText(this.data.parameterFilters.length,42-(w+4),38);
		context.fillText  (this.data.parameterFilters.length,42-(w+4),38);
	}
};

net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex.prototype.fetched=function()
{
	var i;

	for(i=0;i<this.data.parameterFilters.length;i++)
	{
		this.renderParameterFilter(this.data.parameterFilters[i]);
	}
	
	this.callSuper(net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex,"fetched",[]);
};

net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex.prototype.renderParameterFilter=function(parameterFilter)
{
	c=$(
		"<div class='consoleParameterFilterListItem'>"+
			"<div class='consoleParameterFilterListItemMatch'>"+parameterFilter.hrMatch+"</div>"+
			"<div class='consoleParameterFilterListItemDetails'>"+parameterFilter.name+"</div>"+
		    "<div class='consoleParameterFilterListItemDetails'>"+
		        parameterFilter.description+
		        ((parameterFilter.hasClientSide)?("<br/><i>Has client-side capabilities.</i>"):(""))+
		    "</div>"+
		"</div>"
	);
	
	this.parent.append(c);
};

net.lugdunon.world.defaults.compendium.consoleParameterFilters.Codex.prototype.highlight=function(data)
{
	;
};