package net.lugdunon.world.defaults.compendium.consoleParameterFilters;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.lugdunon.command.core.console.parameters.IParameterFilter;
import net.lugdunon.state.State;
import net.lugdunon.state.compendium.rest.BaseCodexRESTHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Collections;

public class CodexRESTHandler extends BaseCodexRESTHandler
{
	@Override
    public String getName()
    {
	    return("Console Parameter Filters");
    }

	@Override
    public String getDescription()
    {
	    return("Provides a complete accounting of this server's available console parameter filters.");
    }

	@Override
    protected JSONObject handleGet(String context, HttpServletRequest request) throws JSONException
    {
		List<String>       cpFilters=new ArrayList<String>(State.instance().getInterpreter().listConsoleParameterFilters());
		JSONObject         o        =new JSONObject();
		JSONArray          a        =new JSONArray ();
		JSONObject         p;
		
		o.put("parameterFilters",a);
		
		Collections.sort(cpFilters);
		
		for(String cpFilter:cpFilters)
		{
			IParameterFilter ipf=State.instance().getInterpreter().getConsoleParameterFilter(cpFilter);
			
			p=new JSONObject();

			p.put("name",         ipf.getName       ());
			p.put("description",  ipf.getDescription());
			p.put("match",        ipf.getMatch      ());
			p.put("hrMatch",      ipf.getHRMatch    ());
			p.put("hasClientSide",ipf.hasClientSide ());
			
			a.put(p);
		}
		
	    return(o);
    }
}
