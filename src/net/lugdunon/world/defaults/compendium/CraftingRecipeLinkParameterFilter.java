package net.lugdunon.world.defaults.compendium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.core.console.parameters.IParameterFilter;
import net.lugdunon.command.core.console.parameters.RegexBasedParameterFilter;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.compendium.Compendium;
import net.lugdunon.state.recipe.Recipe;

public class CraftingRecipeLinkParameterFilter extends RegexBasedParameterFilter implements IParameterFilter
{
	@Override
	public String getName()
	{
		return("Crafting Recipe Link");
	}

	@Override
	public String getDescription()
	{
		Compendium c=State.instance().getWorld().getCompendium();
		
		return("Replaces text with a link to the specified crafting recipe's entry in the '"+c.getCodex("crafting.recipes").getName()+"' codex of the '"+c.getName()+"'.");
	}

	@Override
	public String getMatch()
	{
		return("(RECIPE:)([0-9A-Za-z\\.]+[0-9A-Za-z]+)");
	}

	@Override
	public String getHRMatch()
	{
		return("RECIPE:RECIPE.ID");
	}
	
	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public String postProcess(String command, Character actor, CommandProperties props)
    {
		Pattern      p =getPattern(       );
	    Matcher      m =p.matcher (command);
	    StringBuffer sb=new StringBuffer ();
	    Recipe       r;
	    
	    while(m.find())
	    {
	    	r=State.instance().getWorld().getRecipes().getRecipe(m.group(2));
	    	
	    	if(r != null)
	    	{
		        m.appendReplacement(
		        	sb, 
		        	"<a onclick=\"game.compendium.show('crafting.recipes','"+
		        	r.getRecipeId()+
		        	"');\">Recipe: "+
		        	State.instance().getWorld().getItemDefinitions().getItemDef(
		        		r.getResult().getItemId()
		        	).getName()+
		        	"</a>"
		        );
	    	}
	    }
	    
		return(sb.toString());
    }
}
